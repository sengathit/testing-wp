<footer class="footer">
    <div class="container">
        <div class="row mb-2">
            <div class="col-md-8">
                <?php 
                    // MC Options
                    $mcFormHeading = get_field('mc_form_heading', 'option');
                    $mcFormContent = get_field('mc_form_content', 'option');
                ?>
                <h5><?php echo $mcFormHeading; ?></h5>
                <p><?php echo $mcFormContent; ?></p>

                <form action="" method="POST">
                    <div class="mb-3">
                        <input type="email" name="footer_cof_email" placeholder="Enter email address" onsubmit="return validateMyForm();" class="form-control">
                        <!-- honeypot field start-->
                            <div style="display:none;">
                                <input type="text" name="hidden" id="hidden" />
                            </div>
                        <!-- honeypot field end -->
                    </div>
                    <div class="mb-3">    
                        <button class="btn btn-primary btn-sm" type="submit">Signup Now</button>
                    </div>
                </form>
            </div>
            <div class="col-md-2">
                <h5>Our Company</h5>
                <?php
                    wp_nav_menu( array(
                        'theme_location' => 'footer-menu', // Defined when registering the menu
                        'menu_id'        => 'footer-menu',
                        'container'      => false,
                        'depth'          => 2,
                        'menu_class'     => 'navbar-nav ml-auto small',
                        'walker'         => '', // This controls the display of the Bootstrap Navbar
                        'fallback_cb'    => '', // For menu fallback
                    ) );
                ?>
            </div>
            <div class="col-md-2">
                <?php 
                    $companyLogo = get_field('company_logo', 'option');
                    $siteName = get_bloginfo();
                    $siteUrl = get_site_url();
                    echo '<a href="' . $siteUrl . '">';
                    //echo '<pre>'; print_r($optionsCF); echo '</pre>';
                    if( !empty($companyLogo) ) {
                        echo '<!-- Logo Option -->';
                        echo '<a href=""><img class="img-fluid" src="' . $companyLogo . '" alt="' . $siteName . ' Logo"/></a>'; 
                    } else {
                        echo '<!-- Logo Option Placeholder -->';
                        echo '<a href="' . $siteUrl . '"><img class="img-fluid" src="http://via.placeholder.com/250x76" alt="' . $siteName . ' Logo"/></a>'; 
                    }
                    echo '</a>';
                ?>
            </div>
        </div>
    </div>

    <div class="container footerBottom">
        <div class="row mb-2 no-gutters">
            <?php 
                $siteName = get_bloginfo();
                $siteUrl = get_site_url();
            ?>
            <div class="col-md-6">
                <small>&nbsp; 2009 – <?php echo date('Y'); ?> <a href="<?php echo $siteUrl; ?>"><?php echo $siteName; ?></a>. All rights reserved.</small>
            </div>
            <div class="col-md-6 text-right">
                <small>Website Designed &amp; Developed by <a href="https://eightythreecreative.com" target="_blank" title="Website designed and developed by Eighty Three Creative, Inc.">Eighty Three Creative, Inc.</a></small>
            </div>
        </div>
    </div>
</footer>

<?php wp_footer(); ?>

<?php
    $googleAnalytics = get_field('google_analytics', 'option');
    if( !empty($googleAnalytics) ) {
        echo $googleAnalytics; 
    }
?>

</body>
</html>

