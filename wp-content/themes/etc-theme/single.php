<?php get_header(); ?>

<main id="pageSingle" roll="main">
	<section itemscope itemtype="http://schema.org/Article">
		<?php if (have_posts()) : ?>

			<?php while (have_posts()) : the_post(); ?>
				<h1 itemprop="name"><?php the_title(); ?></h1>
				<p><small>By <?php the_author(); ?> on <?php the_time('F jS, Y'); ?>  in <?php the_category(', '); ?></small></p>
				<?php the_content(); ?>
			<?php endwhile; ?>

		<?php endif; ?>	
	</section>
</main>

<?php get_footer(); ?>
