j = jQuery.noConflict();

j(window).on('load', function()
{
	/*===== description =====*/
	
});

j(window).on('resize', function()
{
	/*===== description =====*/
	
});

j(window).on('scroll', function()
{
	
	/*===== description =====*/
	
});

j(window).on('load resize scroll', function()
{
	
	/*===== lightbox confirmation =====*/
	
	if(window.location.href.indexOf('action=lightbox') > -1)
	{
		console.log('yea');
		setTimeout(function()
		{
			j('#lightbox').addClass('active');
			j('#formResponse').modal('show');
		},
		1000);
	}
	
    /*===== wrapper padding =====*/
    
    var navHeight = j('header.full .container.fixed').outerHeight();
    j('.wrapper header.full #backgroundContainer').css(
    {
        'margin-top': navHeight
    });
    
    var nav = j('nav, #navigation');
    var navOHeight = j(nav).outerHeight();
    var bod = j('body');
    j(bod).css('margin-top', navOHeight+'px');
	

	/*===== minicart functions =====*/
	
	// Offset
	
	var miniCartOffset = j(window).width() - (j('#navigation li.cart').offset().left + j('#navigation li.cart').width());
	j('#wooMiniCart').css('margin-right', (miniCartOffset - 35)+'px');
	
	// Button
	
	j('#wooMiniCart .buttons a').addClass('btn btn-primary').removeClass('button');
	
});

j(function()
{

	j("input[name=billing_country]").attr('readonly', 'readonly');
	// j('.sub-menu').addClass('hs-mega-menu u-header__sub-menu u-header__mega-menu-width-v1 u-header__mega-menu-wrapper-v2 animated hs-position-left fadeOut');

	/*===== navigation sub menu =====*/
     
	var navContainer = j('nav #mainMenu li.menu-item-has-children');
	navContainer.click(function(event)
	{
		event.preventDefault();
		// j('nav #mainMenu li').removeClass('active').find('ul.sub-menu').hide();
		var bodyWrapper = j('.navOverlay');
		if(j(this).hasClass('active'))
		{
			bodyWrapper.fadeOut('fast');
			j(this).removeClass('active').find('ul.sub-menu').slideUp('fast');
		}
		else
        {
            bodyWrapper.fadeIn('fast');
            j('nav #mainMenu li').removeClass('active').find('ul.sub-menu').hide();
            j(this).addClass('active').find('ul.sub-menu').slideDown('fast');    
        }
    });

    j(window).scroll(function()
    {
        if(j(this).scrollTop() >= 20)
        {
            j('.navOverlay').fadeOut('fast');
            // j('nav #mainMenu li.menu-item-has-children').removeClass('active').find('ul.sub-menu').slideUp('fast');
        }
    });

    j('.navOverlay').click(function()
    {
        j(this).fadeOut('fast');
        j('nav #mainMenu li.menu-item-has-children').removeClass('active').find('ul.sub-menu').slideUp('fast');
    });

    j('li.noLink > a').on('click', function(e) {
    	e.preventDefault();
    });

    j("nav #mainMenu li.menu-item-has-children .menu-item").click(function(e)
    {
        e.stopPropagation();
    });

    j('#mobileNavigation ul li.menu-item-has-children > a').each(function() {
    	console.log(this);
    	j(this).prepend('<span class="icon-add"></span>');
    });

    j('#mobileNavigation ul li.menu-item-has-children > a span').on('click', function(e) {
    	e.preventDefault();
    	// j('.sub-menu').slideUp();
    	j(this).toggleClass('active');
    	j(this).parent().next('.sub-menu').slideToggle();

    	if(j(this).hasClass('active')) {
    		j(this).removeClass('icon-add').addClass('icon-minus');
    	} else {
    		if(j(this).attr('class') != 'active') {
    			j(this).removeClass('icon-minus').addClass('icon-add');
    		}
    	}
    	
    });



    var hamburger="hamb";
    var slideNavName="mobileNav";
    var rectangleName="rect";
    var showRect= "showRect";
    var topRectX= "topRectX";
    var hideRectX= "hideRectX";
    var bottomRectX= "bottomRectX";
    j("#hamburger").click(function(event)
    {
        if(j("#mobileNav").hasClass("hidden"))
        {
            j("#"+rectangleName+"1").toggleClass(showRect+" "+topRectX);
            j("#"+rectangleName+"2").toggleClass(showRect+" "+hideRectX);
            j("#"+rectangleName+"3").toggleClass(showRect+" "+bottomRectX);
            j("#mobileNav").removeClass('hidden');

            j('#mobileNav').fadeIn('slow', function()
            {
                classToggle("body", 'offCanvas');
                classToggle("html", 'offCanvas');
                classToggle(".wrapper", 'offCanvas');
                classToggle("header", 'offCanvas');
            });
        }
        else
        {
            if(j("#mobileNav").attr('class')!="hidden")
            {
                j("#"+rectangleName+"1").toggleClass(showRect+" "+topRectX);
                j("#"+rectangleName+"2").toggleClass(showRect+" "+hideRectX);
                j("#"+rectangleName+"3").toggleClass(showRect+" "+bottomRectX);

                j('.wrapper').fadeIn('slow', function()
                {
					classToggle("body", 'offCanvas');
					classToggle("html", 'offCanvas');
					classToggle(".wrapper", 'offCanvas');
					classToggle("header", 'offCanvas');
            	});

            	j('#mobileNav').fadeOut('slow', function()
            	{
            	    classToggle("#mobileNav", 'hidden')
            	});
            }
        }
    });


     /*===== mobile menu option 2 =====*/
     
    j('#menu-mobile-menu li.more').click(function(e)
    {
        e.preventDefault();
        j('body').addClass('noScroll');
        j('#mobileNavTwo').addClass('active').animate(
        {
        	height: '100%'
        }, 400);
        j('#menu-mobile-menu').addClass('active').animate(
        {
        	bottom: '-56px'
        }, 300);

        setTimeout(function()
        {
        	// j('#menu-more-menu').delay(100).show().addClass('animated slideInRight');
            j('#menu-more-menu').removeClass('animated slideOutRight').show().addClass('animated slideInRight');
            j('#mobileNav .closeMenu').removeClass('animated slideOutRight').show().addClass('animated slideInRight');
        }, 400);
    });

    j('#mobileNavTwo .closeMenu').click(function(e)
    {
        e.preventDefault();
        j('body').removeClass('noScroll');
        j('#menu-more-menu').removeClass('animated slideInRight').addClass('animated slideOutRight');
        j(this).removeClass('animated slideInRight').addClass('animated slideOutRight');

        setTimeout(function()
        {
            // j('#menu-more-menu').delay(100).show().addClass('animated slideInRight');

            j('#mobileNav').removeClass('active').animate({
                height: '70px'
            }, 400);
            j('#menu-mobile-menu').addClass('active').animate(
            {
                bottom: '50px'
            }, 300);
        }, 400);
    });

	/*===== carousels =====*/


     
	j('.carousel-slideshow').find('.owl-carousel').owlCarousel(
		{
			items: 1,
			itemElement: 'div.card',
			nav: true,
			navText: ['', ''],
			dots: true,
			navContainer: 'span.pagination',
			autoplay: true,
			autoplayTimeout: 3000,
			autoplayHoverPause: true,
			loop: true,
		}
	);
     
	j('.carousel-categories').find('.owl-carousel').owlCarousel(
		{
			items: 4,
			itemElement: 'div.col',
			responsive:
			{
				0:
				{
					items: 1,
				},
				350:
				{
					items: 2,
				},
				500:
				{
					items: 3,
				},
				800:
				{
					items: 4,
				}
			},
			nav: true,
			navText: ['', ''],
			dots: true,
		}
	);
     
	j('.carousel-categories_home').find('.owl-carousel').owlCarousel(
		{
			items: 3,
			itemElement: 'div.col',
			responsive:
			{
				0:
				{
					items: 1,
				},
				500:
				{
					items: 2,
				},
				700:
				{
					items: 3,
				}
			},
			nav: true,
			navText: ['', ''],
			dots: true,
		}
	);
     
	j('.carousel-products').find('.owl-carousel').owlCarousel(
		{
			items: 4,
			itemElement: 'div.col',
			responsive:
			{
				0:
				{
					items: 1,
				},
				400:
				{
					items: 2,
				},
				700:
				{
					items: 3,
				},
				1000:
				{
					items: 4,
				}
			},
			nav: true,
			navText: ['', ''],
			dots: true,
		}
	);
     
	j('.carousel-social').find('.owl-carousel').owlCarousel(
		{
			items: 3,
			itemElement: 'div.col',
			responsive:
			{
				0:
				{
					items: 1,
				},
				400:
				{
					items: 2,
				},
				600:
				{
					items: 3,
				}
			},
			nav: true,
			navText: ['', ''],
			dots: true,
		}
	);


	/*===== quantity -/+ =====*/
	
	j('.input-group-text').each(function()
	{
		j(this).on('click', function()
		{
			var control = j(this);
			var pre_val = j(this).closest('.quantity').find('input.qty').val();
			//console.log(pre_val);
			if(control.text() == '+')
			{
				var post_val = parseFloat(pre_val) + 1;
			}
			else 
			{
				if(pre_val > 1)
				{
					var post_val = parseFloat(pre_val) - 1;
				}
				else
				{
					post_val = 1;
				}
			}
			j(this).closest('.quantity').find('input.qty').val(post_val);
			j('button[name="update_cart"]').removeAttr('disabled');
		});
	});
	
	
	/*===== wooMiniCart Trigger =====*/
	
	j('#navigation').find('#miniCartTrigger').on('click', function(wooMC_Trigger)
	{
		j('#wooMiniCart').toggleClass('active');
		wooMC_Trigger.preventDefault();
	});
	
	
	/*===== lightbox closing trigger =====*/
	
	var close = j('#lightbox').find('.close');
	j(close).on('click', function()
	{
		j(this).closest('section').removeClass('active');
		var uri = window.location.toString();
		var clean_uri = uri.substring(0, uri.indexOf('?'));
		window.history.replaceState({}, document.title, clean_uri);
	});

	j('i.ion.ion-ios-close-empty, #formResponse').on('click', function() {
		var uri = window.location.toString();
		var clean_uri = uri.substring(0, uri.indexOf('?'));
		window.history.replaceState({}, document.title, clean_uri);
	});
	
});


/*===== JavaScript =====*/

var googleMap = document.getElementById('googleMap');
if(googleMap)
{
	function initMap()
	{
		var map = new google.maps.Map(googleMap,
		{
			zoom: 2,
			center:
			{
				lat: 10,
				lng: 0
			},
			zoomControl: true,
			zoomControlOptions:
			{
			    position: google.maps.ControlPosition.LEFT_CENTER
			},
			mapTypeControl: false,
			scaleControl: false,
			streetViewControl: false,
			rotateControl: false,
			fullscreenControl: false,
			styles:
			[
				{
				"featureType": "administrative",
				"elementType": "labels.text.fill",
				"stylers": [
				{
				"color": "#467580"
				}
				]
				},
				{
				"featureType": "landscape",
				"elementType": "all",
				"stylers": [
				{
				"color": "#e4ffff"
				}
				]
				},
				{
				"featureType": "poi",
				"elementType": "all",
				"stylers": [
				{
				"visibility": "off"
				}
				]
				},
				{
				"featureType": "road",
				"elementType": "all",
				"stylers": [
				{
				"saturation": -100
				},
				{
				"lightness": 45
				}
				]
				},
				{
				"featureType": "road.highway",
				"elementType": "all",
				"stylers": [
				{
				"visibility": "simplified"
				}
				]
				},
				{
				"featureType": "road.arterial",
				"elementType": "labels.icon",
				"stylers": [
				{
				"visibility": "off"
				}
				]
				},
				{
				"featureType": "transit",
				"elementType": "all",
				"stylers": [
				{
				"visibility": "off"
				}
				]
				},
				{
				"featureType": "water",
				"elementType": "all",
				"stylers": [
				{
				"color": "#9dd9e8"
				},
				{
				"visibility": "on"
				}
				]
				}
			]
		});
		var iconBase = 'https://dev2.83creativegroup.com/fruitoftheearth/website/wp-content/themes/foteboot/assets/img/';
		
		
		var content_l1 = '<p class="m-0">Dallas, TX</p>';
		var window_l1 = new google.maps.InfoWindow(
		{
			content: content_l1
		});
		var marker_l1 = new google.maps.Marker(
		{
			position:
			{
				lat: 32.7767,
				lng: -96.7970,
			},
			map: map,
			title: 'Dallas',
			icon: iconBase+'icon-pin.png',
		});
		marker_l1.addListener('click', function()
		{
			window_l1.open(map, marker_l1);
		});
		
		
		var content_l2 = '<p class="m-0">Los Angeles, CA</p>';
		var window_l2 = new google.maps.InfoWindow(
		{
			content: content_l2
		});
		var marker_l2 = new google.maps.Marker(
		{
			position:
			{
				lat: 34.0522,
				lng: -118.2437,
			},
			map: map,
			title: 'Fort Worth',
			icon: iconBase+'icon-pin.png',
		});
		marker_l2.addListener('click', function()
		{
			window_l2.open(map, marker_l2);
		});

		var content_l3 = '<p class="m-0">Canberra, Australia</p>';
		var window_l3 = new google.maps.InfoWindow(
		{
			content: content_l3
		});
		var marker_l3 = new google.maps.Marker(
		{
			position:
			{
				lat: -25.2744,
				lng: 133.7751,
			},
			map: map,
			title: 'Canberra',
			icon: iconBase+'icon-pin.png',
		});
		marker_l3.addListener('click', function()
		{
			window_l3.open(map, marker_l3);
		});

		var content_l4 = '<p class="m-0">Luanda, Angola</p>';
		var window_l4 = new google.maps.InfoWindow(
		{
			content: content_l4
		});
		var marker_l4 = new google.maps.Marker(
		{
			position:
			{
				lat: -11.2027,
				lng: 17.8739,
			},
			map: map,
			title: 'Luanda',
			icon: iconBase+'icon-pin.png',
		});
		marker_l4.addListener('click', function()
		{
			window_l4.open(map, marker_l4);
		});

		var content_l5 = '<p class="m-0">Dhaka, Bangladesh</p>';
		var window_l5 = new google.maps.InfoWindow(
		{
			content: content_l5
		});
		var marker_l5 = new google.maps.Marker(
		{
			position:
			{
				lat: 23.6850,
				lng: 90.3563,
			},
			map: map,
			title: 'Dhaka',
			icon: iconBase+'icon-pin.png',
		});
		marker_l5.addListener('click', function()
		{
			window_l5.open(map, marker_l5);
		});

		var content_l6 = '<p class="m-0">Belmopan, Belize</p>';
		var window_l6 = new google.maps.InfoWindow(
		{
			content: content_l6
		});
		var marker_l6 = new google.maps.Marker(
		{
			position:
			{
				lat: 17.1899,
				lng: -88.4976,
			},
			map: map,
			title: 'Belmopan',
			icon: iconBase+'icon-pin.png',
		});
		marker_l6.addListener('click', function()
		{
			window_l6.open(map, marker_l6);
		});

		var content_l7 = '<p class="m-0">Sofia, Bulgaria</p>';
		var window_l7 = new google.maps.InfoWindow(
		{
			content: content_l7
		});
		var marker_l7 = new google.maps.Marker(
		{
			position:
			{
				lat: 42.7339,
				lng: 25.4858,
			},
			map: map,
			title: 'Sofia',
			icon: iconBase+'icon-pin.png',
		});
		marker_l7.addListener('click', function()
		{
			window_l7.open(map, marker_l7);
		});

		var content_l8 = '<p class="m-0">Beijing, China</p>';
		var window_l8 = new google.maps.InfoWindow(
		{
			content: content_l8
		});
		var marker_l8 = new google.maps.Marker(
		{
			position:
			{
				lat: 39.9042,
				lng: 116.4074,
			},
			map: map,
			title: 'Beijing',
			icon: iconBase+'icon-pin.png',
		});
		marker_l8.addListener('click', function()
		{
			window_l8.open(map, marker_l8);
		});

		var content_l9 = '<p class="m-0">Paris, France</p>';
		var window_l9 = new google.maps.InfoWindow(
		{
			content: content_l9
		});
		var marker_l9 = new google.maps.Marker(
		{
			position:
			{
				lat: 46.2276,
				lng: 2.2137,
			},
			map: map,
			title: 'Paris',
			icon: iconBase+'icon-pin.png',
		});
		marker_l9.addListener('click', function()
		{
			window_l9.open(map, marker_l9);
		});

		var content_l10 = '<p class="m-0">Hagåtña, Guam</p>';
		var window_l10 = new google.maps.InfoWindow(
		{
			content: content_l10
		});
		var marker_l10 = new google.maps.Marker(
		{
			position:
			{
				lat: 13.4443,
				lng: 144.7937,
			},
			map: map,
			title: 'Hagåtña',
			icon: iconBase+'icon-pin.png',
		});
		marker_l10.addListener('click', function()
		{
			window_l10.open(map, marker_l10);
		});

		var content_l11 = '<p class="m-0">Central, Hong Kong</p>';
		var window_l11 = new google.maps.InfoWindow(
		{
			content: content_l11
		});
		var marker_l11 = new google.maps.Marker(
		{
			position:
			{
				lat: 22.3964,
				lng: 114.1095,
			},
			map: map,
			title: 'Central',
			icon: iconBase+'icon-pin.png',
		});
		marker_l11.addListener('click', function()
		{
			window_l11.open(map, marker_l11);
		});

		var content_l12 = '<p class="m-0">Reykjavik, Iceland</p>';
		var window_l12 = new google.maps.InfoWindow(
		{
			content: content_l12
		});
		var marker_l12 = new google.maps.Marker(
		{
			position:
			{
				lat: 64.1265,
				lng: -21.8174,
			},
			map: map,
			title: 'Reykjavik',
			icon: iconBase+'icon-pin.png',
		});
		marker_l12.addListener('click', function()
		{
			window_l12.open(map, marker_l12);
		});

		var content_l13 = '<p class="m-0">New Delhi, India</p>';
		var window_l13 = new google.maps.InfoWindow(
		{
			content: content_l13
		});
		var marker_l13 = new google.maps.Marker(
		{
			position:
			{
				lat: 20.5937,
				lng: 78.9629,
			},
			map: map,
			title: 'New Delhi',
			icon: iconBase+'icon-pin.png',
		});
		marker_l13.addListener('click', function()
		{
			window_l13.open(map, marker_l13);
		});

		var content_l14 = '<p class="m-0">Dublin, Ireland</p>';
		var window_l14 = new google.maps.InfoWindow(
		{
			content: content_l14
		});
		var marker_l14 = new google.maps.Marker(
		{
			position:
			{
				lat: 53.3498,
				lng: -6.2603,
			},
			map: map,
			title: 'Dublin',
			icon: iconBase+'icon-pin.png',
		});
		marker_l14.addListener('click', function()
		{
			window_l14.open(map, marker_l14);
		});

		var content_l15 = '<p class="m-0">Seoul, So. Korea</p>';
		var window_l15 = new google.maps.InfoWindow(
		{
			content: content_l15
		});
		var marker_l15 = new google.maps.Marker(
		{
			position:
			{
				lat: 35.9078,
				lng: 127.7669,
			},
			map: map,
			title: 'Seoul',
			icon: iconBase+'icon-pin.png',
		});
		marker_l15.addListener('click', function()
		{
			window_l15.open(map, marker_l15);
		});

		var content_l16 = '<p class="m-0">Tripoli, Libya</p>';
		var window_l16 = new google.maps.InfoWindow(
		{
			content: content_l16
		});
		var marker_l16 = new google.maps.Marker(
		{
			position:
			{
				lat: 26.3351,
				lng: 17.2283,
			},
			map: map,
			title: 'Tripoli',
			icon: iconBase+'icon-pin.png',
		});
		marker_l16.addListener('click', function()
		{
			window_l16.open(map, marker_l16);
		});

		var content_l17 = '<p class="m-0">Vilnius, Lithuania</p>';
		var window_l17 = new google.maps.InfoWindow(
		{
			content: content_l17
		});
		var marker_l17 = new google.maps.Marker(
		{
			position:
			{
				lat: 55.1694,
				lng: 23.8813,
			},
			map: map,
			title: 'Vilnius',
			icon: iconBase+'icon-pin.png',
		});
		marker_l17.addListener('click', function()
		{
			window_l17.open(map, marker_l17);
		});

		var content_l18 = '<p class="m-0">Skopje, Macedonia</p>';
		var window_l18 = new google.maps.InfoWindow(
		{
			content: content_l18
		});
		var marker_l18 = new google.maps.Marker(
		{
			position:
			{
				lat: 41.6086,
				lng: 21.7453,
			},
			map: map,
			title: 'Skopje',
			icon: iconBase+'icon-pin.png',
		});
		marker_l18.addListener('click', function()
		{
			window_l18.open(map, marker_l18);
		});

		var content_l19 = '<p class="m-0">Kuala Lumpur, Malaysia</p>';
		var window_l19 = new google.maps.InfoWindow(
		{
			content: content_l19
		});
		var marker_l19 = new google.maps.Marker(
		{
			position:
			{
				lat: 4.2105,
				lng: 101.9758,
			},
			map: map,
			title: 'Kuala Lumpur',
			icon: iconBase+'icon-pin.png',
		});
		marker_l19.addListener('click', function()
		{
			window_l19.open(map, marker_l19);
		});

		var content_l20 = '<p class="m-0">Dubai</p>';
		var window_l20 = new google.maps.InfoWindow(
		{
			content: content_l20
		});
		var marker_l20 = new google.maps.Marker(
		{
			position:
			{
				lat: 25.2048,
				lng: 55.2708,
			},
			map: map,
			title: 'Dubai',
			icon: iconBase+'icon-pin.png',
		});
		marker_l20.addListener('click', function()
		{
			window_l20.open(map, marker_l20);
		});

		var content_l21 = '<p class="m-0">Ulaanbaatar, Mongolia</p>';
		var window_l21 = new google.maps.InfoWindow(
		{
			content: content_l21
		});
		var marker_l21 = new google.maps.Marker(
		{
			position:
			{
				lat: 46.8625,
				lng: 103.8467,
			},
			map: map,
			title: 'Ulaanbaatar',
			icon: iconBase+'icon-pin.png',
		});
		marker_l21.addListener('click', function()
		{
			window_l21.open(map, marker_l21);
		});

		var content_l22 = '<p class="m-0">Amsterdam, The Netherlands</p>';
		var window_l22 = new google.maps.InfoWindow(
		{
			content: content_l22
		});
		var marker_l22 = new google.maps.Marker(
		{
			position:
			{
				lat: 52.1326,
				lng: 5.2913,
			},
			map: map,
			title: 'Amsterdam',
			icon: iconBase+'icon-pin.png',
		});
		marker_l22.addListener('click', function()
		{
			window_l22.open(map, marker_l22);
		});

		var content_l23 = '<p class="m-0">Wellington, New Zealand</p>';
		var window_l23 = new google.maps.InfoWindow(
		{
			content: content_l23
		});
		var marker_l23 = new google.maps.Marker(
		{
			position:
			{
				lat: -40.9006,
				lng: 174.8860,
			},
			map: map,
			title: 'Wellington',
			icon: iconBase+'icon-pin.png',
		});
		marker_l23.addListener('click', function()
		{
			window_l23.open(map, marker_l23);
		});

		var content_l24 = '<p class="m-0">Avarua, Cook Islands</p>';
		var window_l24 = new google.maps.InfoWindow(
		{
			content: content_l24
		});
		var marker_l24 = new google.maps.Marker(
		{
			position:
			{
				lat: -21.2367,
				lng: -159.7777,			},
			map: map,
			title: 'Avarua',
			icon: iconBase+'icon-pin.png',
		});
		marker_l24.addListener('click', function()
		{
			window_l24.open(map, marker_l24);
		});

		var content_l25 = '<p class="m-0">Abuja, Nigeria</p>';
		var window_l25 = new google.maps.InfoWindow(
		{
			content: content_l25
		});
		var marker_l25 = new google.maps.Marker(
		{
			position:
			{
				lat: 9.0820,
				lng: 8.6753,			},
			map: map,
			title: 'Abuja',
			icon: iconBase+'icon-pin.png',
		});
		marker_l25.addListener('click', function()
		{
			window_l25.open(map, marker_l25);
		});

		var content_l26 = '<p class="m-0">Islamabad, Pakistan</p>';
		var window_l26 = new google.maps.InfoWindow(
		{
			content: content_l26
		});
		var marker_l26 = new google.maps.Marker(
		{
			position:
			{
				lat: 30.3753,
				lng: 69.3451,			},
			map: map,
			title: 'Islamabad',
			icon: iconBase+'icon-pin.png',
		});
		marker_l26.addListener('click', function()
		{
			window_l26.open(map, marker_l26);
		});

		var content_l27 = '<p class="m-0">Manila, Philippines</p>';
		var window_l27 = new google.maps.InfoWindow(
		{
			content: content_l27
		});
		var marker_l27 = new google.maps.Marker(
		{
			position:
			{
				lat: 12.8797,
				lng: 121.7740,			},
			map: map,
			title: 'Manila',
			icon: iconBase+'icon-pin.png',
		});
		marker_l27.addListener('click', function()
		{
			window_l27.open(map, marker_l27);
		});

		var content_l28 = '<p class="m-0">San Juan, Puerto Rico</p>';
		var window_l28 = new google.maps.InfoWindow(
		{
			content: content_l28
		});
		var marker_l28 = new google.maps.Marker(
		{
			position:
			{
				lat: 18.2208,
				lng: -66.5901,			},
			map: map,
			title: 'San Juan',
			icon: iconBase+'icon-pin.png',
		});
		marker_l28.addListener('click', function()
		{
			window_l28.open(map, marker_l28);
		});

		var content_l29 = '<p class="m-0">Singapore</p>';
		var window_l29 = new google.maps.InfoWindow(
		{
			content: content_l29
		});
		var marker_l29 = new google.maps.Marker(
		{
			position:
			{
				lat: 1.3521,
				lng: 103.8198,			},
			map: map,
			title: 'Singapore',
			icon: iconBase+'icon-pin.png',
		});
		marker_l29.addListener('click', function()
		{
			window_l29.open(map, marker_l29);
		});

		var content_l30 = '<p class="m-0">Ljubljana, Slovenia</p>';
		var window_l30 = new google.maps.InfoWindow(
		{
			content: content_l30
		});
		var marker_l30 = new google.maps.Marker(
		{
			position:
			{
				lat: 46.1512,
				lng: 14.9955,			},
			map: map,
			title: 'Ljubljana',
			icon: iconBase+'icon-pin.png',
		});
		marker_l30.addListener('click', function()
		{
			window_l30.open(map, marker_l30);
		});

		var content_l31 = '<p class="m-0">Basseterre, St. Kitts / Nevis</p>';
		var window_l31 = new google.maps.InfoWindow(
		{
			content: content_l31
		});
		var marker_l31 = new google.maps.Marker(
		{
			position:
			{
				lat: 17.3578,
				lng: -62.7830,			},
			map: map,
			title: 'Basseterre',
			icon: iconBase+'icon-pin.png',
		});
		marker_l31.addListener('click', function()
		{
			window_l31.open(map, marker_l31);
		});

		var content_l32 = '<p class="m-0">Paramaribo, Surinam</p>';
		var window_l32 = new google.maps.InfoWindow(
		{
			content: content_l32
		});
		var marker_l32 = new google.maps.Marker(
		{
			position:
			{
				lat: 3.9193,
				lng: -56.0278,			},
			map: map,
			title: 'Paramaribo',
			icon: iconBase+'icon-pin.png',
		});
		marker_l32.addListener('click', function()
		{
			window_l32.open(map, marker_l32);
		});

		var content_l33 = '<p class="m-0">Taipei, Taiwan</p>';
		var window_l33 = new google.maps.InfoWindow(
		{
			content: content_l33
		});
		var marker_l33 = new google.maps.Marker(
		{
			position:
			{
				lat: 23.6978,
				lng: 120.9605,			},
			map: map,
			title: 'Taipei',
			icon: iconBase+'icon-pin.png',
		});
		marker_l33.addListener('click', function()
		{
			window_l33.open(map, marker_l33);
		});

		var content_l34 = '<p class="m-0">Bangkok, Thailand</p>';
		var window_l34 = new google.maps.InfoWindow(
		{
			content: content_l34
		});
		var marker_l34 = new google.maps.Marker(
		{
			position:
			{
				lat: 15.8700,
				lng: 100.9925,			},
			map: map,
			title: 'Bangkok',
			icon: iconBase+'icon-pin.png',
		});
		marker_l34.addListener('click', function()
		{
			window_l34.open(map, marker_l34);
		});

		var content_l35 = '<p class="m-0">Port of Spain, Trinidad & Tobago</p>';
		var window_l35 = new google.maps.InfoWindow(
		{
			content: content_l35
		});
		var marker_l35 = new google.maps.Marker(
		{
			position:
			{
				lat: 10.6918,
				lng: -61.2225,			},
			map: map,
			title: 'Port of Spain',
			icon: iconBase+'icon-pin.png',
		});
		marker_l35.addListener('click', function()
		{
			window_l35.open(map, marker_l35);
		});

		var content_l36 = '<p class="m-0">London, United Kingdom</p>';
		var window_l36 = new google.maps.InfoWindow(
		{
			content: content_l36
		});
		var marker_l36 = new google.maps.Marker(
		{
			position:
			{
				lat: 55.3781,
				lng: -3.4360,			},
			map: map,
			title: 'London',
			icon: iconBase+'icon-pin.png',
		});
		marker_l36.addListener('click', function()
		{
			window_l36.open(map, marker_l36);
		});

		var content_l37 = '<p class="m-0">Grand Prairie, United States</p>';
		var window_l37 = new google.maps.InfoWindow(
		{
			content: content_l37
		});
		var marker_l37 = new google.maps.Marker(
		{
			position:
			{
				lat: 32.7460,
				lng: -96.9978,			},
			map: map,
			title: 'Grand Prairie',
			icon: iconBase+'icon-pin.png',
		});
		marker_l37.addListener('click', function()
		{
			window_l37.open(map, marker_l37);
		});
		
		
	}
}

function setGetParameter(paramName, paramValue)
{
    var url = window.location.href;
    var hash = location.hash;
    url = url.replace(hash, '');
    if (url.indexOf(paramName + "=") >= 0)
    {
        var prefix = url.substring(0, url.indexOf(paramName));
        var suffix = url.substring(url.indexOf(paramName));
        suffix = suffix.substring(suffix.indexOf("=") + 1);
        suffix = (suffix.indexOf("&") >= 0) ? suffix.substring(suffix.indexOf("&")) : "";
        url = prefix + paramName + "=" + paramValue + suffix;
    }
    else
    {
    if (url.indexOf("?") < 0)
        url += "?" + paramName + "=" + paramValue;
    else
        url += "&" + paramName + "=" + paramValue;
    }
    console.log(url);
    window.location.href = url + hash;
}