<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        
        <?php 
            $siteUrl = get_site_url();
            $googleConsole = get_field('webmaster_tools_google_console', 'option');
            //echo '<pre>'; print_r($optionsCF); echo '</pre>';
            if( !empty($googleConsole) ) {
                echo '<!-- Google Console -->';
                echo $googleConsole; 

            }
        ?>

        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon.ico" type="image/x-icon" />
        <title><?php wp_title( '|', true, 'right' ); ?></title>
        <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/build/minified/style.css">

        <?php wp_head(); ?>
        <?php 
            $hotJar = get_field('hotjar', 'option');
            //echo '<pre>'; print_r($optionsCF); echo '</pre>';
            if( !empty($hotJar) ) {
                echo '<!-- Hot Jar -->';
                echo $hotJar; 
            }
        ?>
        
        <?php 
            $facebookPixel = get_field('facebook_pixel', 'option');
            //echo '<pre>'; print_r($optionsCF); echo '</pre>';
            if( !empty($facebookPixel) ) {
                echo '<!-- FB Pixel -->';
                echo $facebookPixel; 
            }
        ?>
        <script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.js"></script>
        <script>
                window.addEventListener("load", function(){
                window.cookieconsent.initialise({
                  "palette": {
                    "popup": {
                      "background": "#252e39"
                    },
                    "button": {
                      "background": "#14a7d0"
                    }
                  },
                  "content": {
                    "message": "This website uses cookies to ensure you get the best experience on our website, analytics, personalized content and ads.<br/>By continuing to browse this site, you agree to this use.",
                    "href": "<?php echo site_url(); ?>/privacy-policy/"
                  }
                })});
        </script>
    </head>

    <body <?php body_class('fixed-nav'); ?>>  
     
    <header>
        <nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
            
            <?php 
                $companyLogo = get_field('company_logo', 'option');
                $siteName = get_bloginfo();
                $siteUrl = get_site_url();
                echo '<a class="navbar-brand" href="' . $siteUrl . '">';
                //echo '<pre>'; print_r($optionsCF); echo '</pre>';
                if( !empty($companyLogo) ) {
                    echo '<!-- Logo Option -->';
                    echo '<a href="' . $siteUrl . '"><img src="' . $companyLogo . '" alt="' . $siteName . ' Logo"/></a>'; 
                } else {
                    echo '<!-- Logo Option Placeholder -->';
                    echo '<a href="' . $siteUrl . '"><img src="http://via.placeholder.com/250x76" alt="' . $siteName . ' Logo"/></a>'; 
                }
                echo '</a>';
            ?>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarsExampleDefault">
                <?php
                    wp_nav_menu( array(
                        'theme_location' => 'main-menu', // Defined when registering the menu
                        'menu_id'        => 'primary-menu',
                        'container'      => false,
                        'depth'          => 2,
                        'menu_class'     => 'navbar-nav ml-auto',
                        'walker'         => new Bootstrap_NavWalker(), // This controls the display of the Bootstrap Navbar
                        'fallback_cb'    => 'Bootstrap_NavWalker::fallback', // For menu fallback
                    ) );
                ?>
            </div>
        </nav>
    </header>
    


