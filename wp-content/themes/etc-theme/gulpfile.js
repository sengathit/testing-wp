/*===== gulp requires =====*/

var gulp = require('gulp'),
    plumber = require('gulp-plumber'),
    gutil = require('gulp-util'),
    sourcemaps = require('gulp-sourcemaps'),
	sass = require('gulp-sass'),
	autoprefixer = require('gulp-autoprefixer'),
    cleanCSS = require('gulp-clean-css'),
    changed = require('gulp-changed'),
    uglify = require('gulp-uglify'),
    rename = require('gulp-rename'),
    concat = require('gulp-concat'),


/*===== node requires =====*/

    exec = require('child_process').exec,
    util = require('util');


/*===== directories =====*/

var dir_assets = 'assets',
	dir_sass = dir_assets+'/sass',
	dir_css = dir_assets+'/css',
	dir_css_pre = dir_css+'/pre',
	dir_css_min = dir_css+'/min',
	dir_js = dir_assets+'/js',
	dir_dependencies = dir_js+'/dependencies',
	dir_config = 'config';


/*===== sass =====*/

gulp.task('sass', function(){
	return gulp.src(dir_sass+'/style.scss')
		.pipe(plumber())
		.pipe(changed(dir_sass+'/**/*.scss'))
		.pipe(sourcemaps.init({loadMaps: true}))
		.pipe(sass().on('error', sass.logError))
		.pipe(autoprefixer('last 2 version', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'))
		.pipe(gulp.dest(dir_css_pre))
		.pipe(cleanCSS())
		.pipe(sourcemaps.write('/'))
		.pipe(gulp.dest(dir_css_min));
});

// Watch

gulp.task('sass:watch', function(){
	gulp.watch(dir_sass+'/**/*.scss', ['sass']);
});


/*===== script =====*/

gulp.task('script', function(){
	return gulp.src(dir_js+'/scripts.js')
		.pipe(plumber())
		.pipe(sourcemaps.init({loadMaps: true}))
		.pipe(uglify())
		.pipe(rename('scripts.min.js'))
		.pipe(sourcemaps.write('/'))
		.pipe(gulp.dest(dir_js));
});

// Watch

gulp.task('script:watch', function(){
	gulp.watch(dir_js+'/scripts.js', ['script']);
});


/*===== dependencies =====*/

gulp.task('dependencies', function(){
	return gulp.src(dir_dependencies+'/*.js')
		.pipe(plumber())
		.pipe(changed(dir_dependencies+'/*.js'))
		.pipe(sourcemaps.init({loadMaps: true}))
		.pipe(concat('dependencies.js'))
		.pipe(uglify())
		.pipe(sourcemaps.write('/'))
		.pipe(gulp.dest(dir_js));
});

// Watch

gulp.task('dependencies:watch', function(){
	gulp.watch(dir_dependencies+'/*.js', ['dependencies']);
});


/*===== gulp task =====*/

gulp.task('default', ['sass', 'sass:watch', 'script', 'script:watch', 'dependencies', 'dependencies:watch']);
