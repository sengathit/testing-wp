<?php 

function lp_mailer() {

	$output = $_POST;
	/*===== Semder =====*/
		
	$sender = $_POST['email_address'];					// or; Example: $_GET/$_POST['email_address'];
	
	/*===== Settings =====*/
	
	$recipients = array(
		'dlucnguyen@eightythreecreative.com',
		// $sender,
		// more addresses,
	);
	$to = implode(',', $recipients);
	$bcc_recipients = array(
		// 'dlucnguyen@eightythreecreative.com',
		// 'bsteiner@eightythreecreative.com',
		//more
	);
	$bcc_to = implode(',', $bcc_recipients);
	$subject = 'Landing Page Contact';						// or; Example: $_GET/$_POST['subject'];
	$from_name = 'Eighty Three Creative, Inc.';			// or; Example: $_GET/$_POST['name'];
	$from_email = 'dlucnguyen@eightythreecreative.com';		// or; Example: $_GET/$_POST['email'];
	$boundary = md5(uniqid(time()));
	
	/*===== Header =====*/
	
    $headers = 'MIME-Version: 1.0'.PHP_EOL; 
	$headers .= 'From: '.strip_tags($from_name).' <'.strip_tags($from_email).'>'.PHP_EOL;
	if(isset($bcc_to) && !empty($bcc_to)){
		$headers .= 'Bcc: '.$bcc_to.PHP_EOL;
	}
    $headers .= 'Content-Type: multipart/mixed; boundary = '.$boundary.PHP_EOL.PHP_EOL;	
	
	/*===== Body =====*/
	
	$body = '<html>';
		$body .= '<body>';
			$body .= '<table width="100%" cellspacing="0" cellpadding="15" border="1" style="font-family:Arial; font-size:13px; color:#333333; letter-spacing:0.0125em; line-height:1.5; margin-bottom:20px; border:1px solid #dddddd; border-collapse:collapse; -webkit-font-smoothing:antialiased;">';
				
				/*===== Data Loop =====*/
				
				foreach($output as $key => $value)
				{
					
					/*===== Security =====*/
					
					$key = trim($key);
					$key = strip_tags($key);
					$key = stripslashes($key);
					$key = htmlspecialchars($key);
					
					if(!is_array($value)){
						$value = trim($value);
						$value = strip_tags($value);
						$value = stripslashes($value);
						$value = htmlspecialchars($value);
					}
					
					/*===== Field Value Checkpoint =====*/
					
					if($key != 'g-recaptcha-response' && !empty($value))
					{
						
						/*===== Label =====*/
						
						$label = ucwords(str_replace('_', ' ', $key));
						
						/*===== Multiselect =====*/
						
						if(is_array($value)){
							$options = implode(', ', $value);
							$body .= '<tr style="border-bottom:1px solid #dddddd;">';
								$body .= '<td style="border-right:1px solid #dddddd;" width="20%" valign="top"><strong>'.$label.'</strong></td>';
								$body .= '<td width="80%" valign="top">'.$options.'</td>';
							$body .= '</tr>';
						}
						
						/*===== Default =====*/
						
						else{
							$body .= '<tr style="border-bottom:1px solid #dddddd;">';
								$body .= '<td style="border-right:1px solid #dddddd;" width="20%" valign="top"><strong>'.$label.'</strong></td>';
								$body .= '<td width="80%" valign="top">'.$value.'</td>';
							$body .= '</tr>';
						}
					}
				}
			$body .= '</table>';
		$body .= '</body>';
	$body .= '</html>';
	
	/*===== $body =====*/
	
    $message = '--'.$boundary.PHP_EOL;
    $message .= 'Content-Type: text/html; charset=UTF-8'.PHP_EOL;
    $message .= 'Content-Transfer-Encoding: base64'.PHP_EOL.PHP_EOL; 
    $message .= chunk_split(base64_encode($body));
	
	/*===== Attachment =====*/
	
	if($_FILES){

		/*
		$directory = 'files/';
		if(!file_exists($directory)){
			mkdir($directory, 0775);
		}
		*/
		$files = $_FILES;
		foreach($files as $key => $value){
			
			/*===== Disect =====*/
			
			$file_tmp_name = $_FILES[$key]['tmp_name'];
			$file_name = $_FILES[$key]['name'];
			$file_size = $_FILES[$key]['size'];
			$file_type = $_FILES[$key]['type'];
			$file_error = $_FILES[$key]['error'];
			
			if($file_size >= 1){
				
				/*===== Read =====*/

				$handle = fopen($file_tmp_name, "r");
				$content = fread($handle, $file_size);
				fclose($handle);
				$encoded_content = chunk_split(base64_encode($content));
			
				/*===== Upload (Optional) =====*/
				
				/*
				$pre = strtolower(str_replace(' ', '_', $key));
				$file = basename($file_name);
				$ext = pathinfo($file, PATHINFO_EXTENSION);
				$rename = $pre.'-'.date('Ymd').'-'.uniqid().'.'.$ext;
				move_uploaded_file($file_tmp_name, $directory.$rename);
				*/
				
				/*===== Attach =====*/
				
    			$message .= '--'.$boundary.PHP_EOL;
    			$message .= "Content-Type: $file_type; name=".$file_name.PHP_EOL;
    			$message .= "Content-Disposition: attachment; filename=".$file_name.PHP_EOL;
    			$message .= "Content-Transfer-Encoding: base64".PHP_EOL;
    			$message .= "X-Attachment-Id: ".rand(1000, 99999).PHP_EOL.PHP_EOL; 
    			$message .= $encoded_content; 	
			}
		}
	}
	/*===== Mail =====*/
		
	mail($to, $subject, $message, $headers);

	$redirect = strtok($_SERVER['HTTP_REFERER'], '?').'/?type=lp_submission&position=' . $_POST['position'] . '';
	header('Location: '.$redirect);
	die();
	
}

if(isset($_POST['lp_referal_url'])) {
	// echo 'hello';
	lp_mailer();
}