<?php
$pageID = $post->ID;
$pageCF = get_fields($pageID);

// Content Blocks
$contentModules = $pageCF['content_modules'];

// echo '<pre>'; echo print_r($contentModules); echo '</pre>';

$count = 0;
foreach($contentModules as $key => $module) {
    $moduleName = $module['content_style'];
    
    // echo '<pre>'; echo print_r($module); echo '</pre>';
    include(TEMPLATEPATH . '/vendor/landing-pages/modules/content-blocks/' . $moduleName . '.php');

    $count++;
}