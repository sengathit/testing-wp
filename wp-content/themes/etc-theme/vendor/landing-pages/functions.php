<?php

// REGISTER LOCATIONS
$landing_page_labels = array(
    'name'               => 'Landing Pages',
    'singular_name'      => 'Landing Page',
    'menu_name'          => 'Landing Pages'
);
$landing_page_args = array(
    'labels'             => $landing_page_labels,
    'public'             => true,
    'capability_type'    => 'post',
    'has_archive'        => true,
    'supports'           => array( 'title', 'thumbnail' )
);
register_post_type('lp', $landing_page_args);

include(TEMPLATEPATH . '/vendor/landing-pages/includes/mailer/lp-mailer.php');