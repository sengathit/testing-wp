
<div id="hero" class="container content">
	<div class="row">
		<div class="col seven">
			<div class="logo"><img src="http://dev.83creativegroup.com/wp/eighty-three-v3/wp-content/themes/ETC/assets/images/83_logotype_wh.png" alt="Eighty Three Creative, Inc." /></div>

			<?php 
				$heroHeading = get_field('hero_heading');
				$heroHeadingDescription = get_field('hero_description');
				echo '<h1>' . $heroHeading . '</h1>';
				echo '<p>' . $heroHeadingDescription . '</p>';

				$pageID = $post->ID;

				$getLink = get_the_permalink($pageID);
			?>
		</div>
		<div class="col five last">
			<form action="" method="POST">
				<?php 
					$heroHeading = get_field('form_heading');
					echo '<h3>' . $heroHeading . '</h3>';
				?>
				<?php 
					$type = $_GET['type'];
					$position = $_GET['position'];
					// echo '<pre>'; echo print_r($type); echo '</pre>';
					// echo '<pre>'; echo print_r($position); echo '</pre>';
					if( $type == 'lp_submission' && $_GET['position'] == 'header') {
						echo '<p>Thank you, we will contact you shortly!</p>';
					}
				?>
				<div class="form-input twelve">
					<?php 
						$formFields = get_field('form_input');

						foreach($formFields as $key => $item) {
							$inputName = $item['type']['value'];
							$inputType = $item['input_type']['value'];
							$inputLabel = $item['type']['label'];

							// Required
							// $required = $item['required'];
							if($item['required'] == 1) {
								$required = ' required=required';
							}
 
							if($inputType == 'textarea') {
								echo '<textarea class="col twelve" name="' . $inputName . '" placeholder="' . $inputLabel . '"' . $required . '></textarea>';
							} else {
								echo '<input class="col twelve" type="' . $inputType . '" name="' . $inputName . '" placeholder="' . $inputLabel . '"' . $required . '/>';
							}
							// echo '<pre>'; echo print_r($item); echo '</pre>';
						}
					?>
					
					<input type="hidden" name="position" value="header">
					<input type="hidden" name="lp_referal_url" value="<?php echo $getLink; ?>">
					<input type="submit" value="Contact Us">
				</div>
				
			</form>
		</div>

	</div>
</div>