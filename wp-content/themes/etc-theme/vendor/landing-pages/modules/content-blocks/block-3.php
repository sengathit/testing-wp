<?php 
	$ctaContent = get_field('cta_form_heading');
	$pageID = $post->ID;
	$pageCF = get_fields($pageID);

	$blockThreeArray = array();
	foreach($pageCF['content_modules'] as $item) {
		$blockThreeArray[] = $item;
	}
?>

<section id="contentBlockThree" class="full">
	<div class="container thousandMax">
		<div class="block-3">
			<div class="content">
				<?php 
					// echo '<pre>'; echo print_r($blockThreeArray[$count]); echo '</pre>';
					foreach($blockThreeArray[$count]['rows'] as $item) {
						$icon = file_get_contents($item['icon']);
						$heading = $item['heading'];
						$content = $item['content'];
						$footer = $item['footer'];
						echo '<div class="row">';
						echo '<div class="col three">';
							if(!empty($icon)) {
								echo $icon;
							} else {
								echo '<img src="https://via.placeholder.com/56x56">';
							}
						echo '</div>';
						echo '<div class="col nine last">';
							echo '<h2>' . $heading . '</h2>';
							echo wpautop($content);
							if(!empty($footer)) { echo '<p class="footer">' . $footer . '</p>'; }
						echo '</div>';
						echo '</div>';
					}
				?>
				<p><?php // echo $blockTwoArray[0][0]; ?></p>
			</div>
		</div>
	</div>
</section>