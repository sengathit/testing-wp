<?php 
$pageID = $post->ID;
$pageCF = get_fields($pageID);


$heading = $pageCF['content_modules'][0]['heading'];
$image = $pageCF['content_modules'][0]['image'];
$list = $pageCF['content_modules'][0]['list'];

$listArray = array();
foreach($pageCF['content_modules'] as $key => $item) {
	if($item['content_style'] == 'block-1') {
		
		$listArray[$key] = array(
			$item['heading'],
			$item['columns']
		);
	}
}


?>

<?php // echo '<pre>'; echo print_r($listArray); echo '</pre>'; ?>

<section id="contentBlockOne" class="full">
	<div class="container fourteenHundredMax">
		<div class="block-1">
			<div class="content">
				<h2><?php echo $listArray[$count][0]; ?></h2>
			</div>
			<div class="content content-columns">
				<?php

					$counter = 1; 
					foreach($listArray[0][1] as $item) :
						$heading = $item['column_heading'];
						$subHeading = $item['column_sub_heading'];
						$image = $item['column_image'];
						$link = $item['page_link'];

						if ($counter % 3 == 0) {
							echo '<div class="col four last">';
						} else {
							echo '<div class="col four">';
						}
							echo '<a href="' . $link . '">';
							if(!empty($image)) {
								echo '<img src="' . $image . '" alt="' . $heading . '"/>';
							} else {
								echo '<img src="https://via.placeholder.com/350x350">';
							}
							echo '</a>';

							echo '<h3><a href="' . $link . '">' . $heading . '</a></h3>';
							echo '<p>' . $subHeading . '</p>';
							
						echo '</div>';

						$counter++;
						// echo '<pre>'; echo print_r($item); echo '</pre>';
					endforeach;
				?>
				</div>
			</div>
		</div>
	</div>
</section>