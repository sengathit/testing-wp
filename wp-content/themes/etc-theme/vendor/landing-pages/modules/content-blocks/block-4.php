<?php 
	$ctaContent = get_field('cta_form_heading');
	$pageID = $post->ID;
	$pageCF = get_fields($pageID);

	$blockFourArray = array();
	foreach($pageCF['content_modules'] as $item) {

		if($item['content_style'] == 'block-4') {
			
			$blockFourArray[] = array(
				$item['heading'],
				$item['image'],
				$item['content_widget']
			);
		}
	}
	// 
	$image = $blockFourArray[0][1];
?>

<section id="contentBlockFour" class="full" style="background: #14274a url(<?php echo $image; ?>) no-repeat center right;background-size:contain;">
	<div class="container thousandMax">
		<div class="block-4">
			<div class="content">
				<?php 
					// echo '<pre>'; echo print_r($blockFourArray); echo '</pre>';

					$heading = $blockFourArray[0][0];
					$content = wpautop($blockFourArray[0][2]);
				?>
				<div class="col seven">
					<h2><?php echo $heading; ?></h2>
					<?php echo $content; ?>
				</div>
				<div class="col five last">

				</div>
				<?php 
					
				?>
				<p><?php // echo $blockTwoArray[0][0]; ?></p>
			</div>
		</div>
	</div>
</section>