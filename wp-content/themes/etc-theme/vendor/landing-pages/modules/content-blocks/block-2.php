<?php 
	$ctaContent = get_field('cta_form_heading');
	$pageID = $post->ID;
	$pageCF = get_fields($pageID);

	// 
	$contactNumber = $pageCF['content_modules'][1]['contact_text'];

	$blockTwoArray = array();
	foreach($pageCF['content_modules'] as $item) {

		if($item['content_style'] == 'block-2') {

			$blockTwoArray[] = array(
				$item['content_widget']
			);
		}
	}
	// echo '<pre>'; echo print_r($blockTwoArray); echo '</pre>';
?>

<section id="contentBlockTwo" class="full">
	<div class="container thousandMax">
		<div class="block-2">
			<div class="content">
				<p><?php echo $blockTwoArray[0][0]; ?></p>
			</div>
		</div>
	</div>
</section>