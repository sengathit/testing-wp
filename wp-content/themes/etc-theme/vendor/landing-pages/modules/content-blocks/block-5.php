<?php 
	$ctaContent = get_field('cta_form_heading');
	$pageID = $post->ID;

	$getLink = get_the_permalink($pageID);
	$pageCF = get_fields($pageID);

	// 
	$contactNumber = $pageCF['content_modules'][1]['contact_text'];

	$blockFiveArray = array();
	foreach($pageCF['content_modules'] as $item) {

		if($item['content_style'] == 'block-5') {

			$blockFiveArray[] = $item['heading'];
		}
	}
	// 
?>

<section id="contentBlockFive" class="full">
	<div class="container thousandMax">
		<div class="block-5">
			<div class="content">
				<?php 
					// echo '<pre>'; echo print_r($blockFiveArray); echo '</pre>';
				?>
				<h2><?php echo $blockFiveArray[0]; ?></h2>
				<?php 
					$type = $_GET['type'];
					$position = $_GET['position'];
					// echo '<pre>'; echo print_r($type); echo '</pre>';
					// echo '<pre>'; echo print_r($position); echo '</pre>';
					if( $type == 'lp_submission' && $_GET['position'] == 'body') {
						echo '<p>Thank you, we will contact you shortly!</p>';
					}
				?>
				<form action="" method="POST">
					<div class="form-input col nine">
						<input type="text" name="name" placeholder="Name" class="col four" required=required>
						<input type="text" name="company" placeholder="Company" class="col four" required=required>
						<input type="email" name="email_address" placeholder="Email Address" class="col four last" required=required>
					</div>
					<div class="form-submit col three last">
						<input type="submit" value="Contact Us">
					</div>
					<input type="hidden" name="lp_referal_url" value="<?php echo $getLink; ?>">
					<input type="hidden" name="position" value="body">
				</form>
			</div>
		</div>
	</div>
</section>