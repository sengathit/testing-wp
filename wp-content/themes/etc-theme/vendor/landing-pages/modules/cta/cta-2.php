<?php 
	$ctaContent = get_field('cta_form_heading');
	$ctaContentOne = get_field('cta_1_content');
?>
<div class="container">
	<form action="" method="post">
		<h3><?php echo $ctaContentOne; ?></h3>
		<h2><?php echo $ctaContent; ?></h2>
		<ul>
			<li class="fieldset fieldset-1">
				<ul>
					<li><input type="text" name="first_name" placeholder="First Name *" pattern="[a-z A-Z]*" required="required"></li>
					<li><input type="email" name="email_address" placeholder="Email Address *" required="required"></li>
				</ul>
			</li>
			<li class="fieldset fieldset-2">
				<ul>
					<li><input type="text" name="last_name" placeholder="Last Name *" pattern="[a-z A-Z]*" required="required"></li>
					<li><input type="tel" name="phone_number" placeholder="Phone Number *" required="required"></li>
				</ul>
			</li>
			<li>
				<select name="how_did_you_hear_about_us" required="required">
					<option value="">How did you hear about us?</option>
					<option>Repeat Customer</option>
					<option>Referral</option>
					<option>TV</option>
					<option>Radio</option>
					<option>BBB</option>
					<option>Angie's List</option>
					<option>Good Contractors List</option>
					<option>Consumers Choice Award</option>
					<option>Internet Search</option>
					<option>GuildQuality</option>
					<option>Facebook</option>
					<option>Twitter</option>
					<option>Pinterest</option>
					<option>Nextdoor</option>
					<option>Other</option>
				</select>
			</li>
			<li><div class="g-recaptcha" data-callback="recaptchaCallback" data-sitekey="6LeHnEQUAAAAAErxNU1LZBEWJk7kiKqorn6qPMi6"></div></li>
			
			<li class="button">
				<input type="submit" name="submit" value="Submit" disabled>
			</li>
		</ul>
	</form>
</div>