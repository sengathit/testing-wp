<?php get_header(); ?>

<main id="page" roll="main">
	<section class="container">
		<?php if (have_posts()) : ?>

			<?php while (have_posts()) : the_post(); ?>

				<h1><?php the_title(); ?></h1>

				<?php 
					// Use page slug as your template name
					$pageSlug = $post->post_name;

					if($pageSlug) {
						echo '<pre>'; echo print_r($pageSlug); echo '</pre>';
						get_template_part( 'template-parts/content', $pageSlug );
					}
				?>

			<?php endwhile; ?>

		<?php endif; ?>	
	</section>
</main>

<?php get_footer(); ?>
