<?php

// Register Site Options
if( function_exists('acf_add_options_page') ) {
    
    acf_add_options_page(array(
        'page_title'    => 'Theme General Settings',
        'menu_title'    => 'Site Options',
        'menu_slug'     => 'theme-general-settings',
        'capability'    => 'edit_posts',
        'redirect'      => false
    ));
    
    // acf_add_options_sub_page(array(
    //     'page_title'    => 'Theme Header Settings',
    //     'menu_title'    => 'Header',
    //     'parent_slug'   => 'theme-general-settings',
    // ));
    
    // acf_add_options_sub_page(array(
    //     'page_title'    => 'Theme Footer Settings',
    //     'menu_title'    => 'Footer',
    //     'parent_slug'   => 'theme-general-settings',
    // ));
    
}

// Register Custom Post Types - use ACF to add fields
add_action('init', 'register_custom_posts_init');

function register_custom_posts_init() {
    // REGISTER LOCATIONS
    // $location_labels = array(
    //     'name'               => 'Locations',
    //     'singular_name'      => 'Location',
    //     'menu_name'          => 'Locations'
    // );
    // $locations_args = array(
    //     'labels'             => $location_labels,
    //     'public'             => true,
    //     'capability_type'    => 'post',
    //     'has_archive'        => false,
    //     'supports'           => array( 'title', 'thumbnail' )
    // );
    // register_post_type('locations', $locations_args);

    // REGISTER CAREERS
    // $career_labels = array(
    //     'name'               => 'Careers',
    //     'singular_name'      => 'Career',
    //     'menu_name'          => 'Careers'
    // );
    // $career_args = array(
    //     'labels'             => $career_labels,
    //     'public'             => true,
    //     'capability_type'    => 'post',
    //     'has_archive'        => false,
    //     'supports'           => array( 'title', 'thumbnail' )
    // );
    // register_post_type('careers', $career_args);
}