<?php
	
	/*===== Mailchimp Function =====*/
	
	function mailchimp(){
		$email = $_POST['email_address'];

		$apikey = get_field('mailchimp_api_key', 'option');
        $auth = base64_encode( 'user:'.$apikey );
        $listId = get_field('mailchimp_list_id', 'option');

        $data = array(
            'apikey'        => $apikey,
            'email_address' => $email,
            'status'        => 'subscribed',
            // 'merge_fields'  => array(
            //     // 'FNAME' => $name
            // )
        );
        $json_data = json_encode($data);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://us18.api.mailchimp.com/3.0/lists/' . $listId . '/members/');
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
                                                    'Authorization: Basic '.$auth));
        curl_setopt($ch, CURLOPT_USERAGENT, 'PHP-MCAPI/2.0');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);                                                                                                                  

        $result = curl_exec($ch);
	}
	
	/*===== Mailchimp Gateway =====*/
	
	$robot_check = $_POST['honeypot'];
	$redirect = strtok($_SERVER['HTTP_REFERER'], '?').'/thank-you/?service=mailchimp';
	if(isset($robot_check))
	{
		if(isset($robot_check) && !empty($robot_check))
		{
			header('Location: '.$redirect);
			die();
		}
		elseif(isset($robot_check) && empty($robot_check))
		{
			header('Location: '.$redirect);
			mailchimp();
			die();
		}
	}