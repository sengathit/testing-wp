<?php 

function etc_theme_support() {

	// Switch default core markup for search form, comment form, and comments to output valid HTML5
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Add menu support
	add_theme_support( 'menus' );

	// Let WordPress manage the document title
	add_theme_support( 'title-tag' );

	// Add post thumbnail support: http://codex.wordpress.org/Post_Thumbnails
	add_theme_support( 'post-thumbnails' );

	// RSS thingy
	add_theme_support( 'automatic-feed-links' );

	// Add post formats support: http://codex.wordpress.org/Post_Formats
	add_theme_support( 'post-formats', array('aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio', 'chat') );

	// Declare WooCommerce support per http://docs.woothemes.com/document/third-party-custom-theme-compatibility/
	add_theme_support( 'woocommerce' );
}

add_action( 'after_setup_theme', 'etc_theme_support' );

function etc_excerpt_length( $length ) {
	return 20;
}
add_filter( 'excerpt_length', 'etc_excerpt_length', 999 );

register_nav_menus( array(
    'main-menu' => esc_html__( 'Primary', 'theme-textdomain' ),
    'footer-menu' => esc_html__( 'Footer', 'theme-textdomain' ),
) );

add_filter('default_hidden_meta_boxes', 'show_hidden_meta_boxes', 10, 2);
 
function show_hidden_meta_boxes($hidden, $screen) {
    if ( 'post' == $screen->base ) {
        foreach($hidden as $key=>$value) {
            if ('postexcerpt' == $value) {
                unset($hidden[$key]);
                break;
            }
        }
    }
 
    return $hidden;
}

// Custom login styles
function my_login_logo() { 

	$companyLogo = get_field('company_logo', 'option');
    $siteName = get_bloginfo();
    //echo '<pre>'; print_r($optionsCF); echo '</pre>';
    if( !empty($companyLogo) ) {
    	$loginLogo = $companyLogo;
    } else {
    	$loginLogo = 'http://via.placeholder.com/250x76'; 
    }
	?>
    <style type="text/css">
        #login h1 a, .login h1 a {
        	background-image: url(<?php echo $loginLogo; ?>);

			height: 80px;
			width:320px;
			background-size: contain;
			background-repeat: no-repeat;
        	padding-bottom: 30px;
        }
    </style>
	<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );

// Display Name
/* First name as default display name. */
add_action( 'profile_update', 'set_display_name', 10 );

function set_display_name( $user_id ) {

    $data = get_userdata( $user_id );

    if($data->first_name && $data->last_name) {

        remove_action( 'profile_update', 'set_display_name', 10 ); // profile_update is called by wp_update_user, so we need to remove it before call, to avoid infinite recursion
        wp_update_user( 
            array (
                'ID' => $user_id, 
                'display_name' => "$data->first_name $data->last_name"
            ) 
        );
        add_action( 'profile_update', 'set_display_name', 10 );
    }
}

// Clean Special Characters
function cleanPhoneNumber($string) {
   $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
   $string = str_replace('-', '', $string);
   return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
}