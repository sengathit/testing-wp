<?php 

function register_my_menus() {
    register_nav_menus(
    array( 'main-menu' => __( 'Main Menu' ), 'footer-menu' => __( 'Footer Menu' ), 'top-menu' => __( 'Top Menu' ), 'mobile-menu' => __( 'Mobile Menu' ) )
    );
}
add_action( 'init', 'register_my_menus' );

function etc_register_theme_customizer( $wp_customize ) {

	// Create custom panels
	$wp_customize->add_panel( 'mobile_menu_settings', array(
	  'priority' => 1000,
	  'theme_supports' => '',
	  'title' => __( 'Menu Settings', 'etc' ),
	  'description' => __( 'Controls the mobile menu', 'etc' ),
	) );

	// Create custom field for mobile navigation layout
	$wp_customize->add_section( 'mobile_menu_layout' , array(
		'title'	=> __('Mobile navigation layout','etc'),
		'panel' => 'mobile_menu_settings',
		'priority' => 1000,
	));

	// Set default navigation layout
	$wp_customize->add_setting(
		'wpt_mobile_menu_layout',
		array(
			'default'	=> __( 'topbar', 'etc' ),
		)
	);

	// Add options for navigation layout
	$wp_customize->add_control(
		new WP_Customize_Control(
			$wp_customize,
			'mobile_menu_layout',
			array(
				'type'		=> 'radio',
				'section' 	=> 'mobile_menu_layout',
				'settings' 	=> 'wpt_mobile_menu_layout',
		        'choices' => array(
					'standard' => 'Standard',
		            'premium' => 'Premium',
		        ),
			)
		)
	);

	// Menu Positioning
	$wp_customize->add_panel( 'menu_settings', array(
	  'priority' => 1000,
	  'theme_supports' => '',
	  'title' => __( 'Menu Settings', 'etc' ),
	  'description' => __( 'Controls the menu', 'etc' ),
	) );

	// Create custom field for mobile navigation layout
	$wp_customize->add_section( 'menu_layout' , array(
		'title'	=> __('Menu layout','etc'),
		'panel' => 'mobile_menu_settings',
		'priority' => 1000,
	));

	// Set default navigation layout
	$wp_customize->add_setting(
		'etc_menu_layout',
		array(
			'default'	=> __( 'topbar', 'etc' ),
		)
	);

	// Add options for navigation layout
	$wp_customize->add_control(
		new WP_Customize_Control(
			$wp_customize,
			'menu_layout',
			array(
				'type'		=> 'radio',
				'section' 	=> 'menu_layout',
				'settings' 	=> 'etc_menu_layout',
				'label'	=> 'Select Menu Position Attachment',
		        'choices' => array(
					'standard' => 'Standard',
		            'fixed' => 'Fixed',
		        ),
			)
		)
	);

	// Set main nav background color
	$wp_customize->add_setting(
		'etc_menu_bg_color',
		array(
			'default'	=> __( 'white', 'etc' ),
		)
	);

	// Add options for navigation layout
	$wp_customize->add_control(
		new WP_Customize_Control(
			$wp_customize,
			'etc_menu_bg_color',
			array(
				'type'		=> 'select',
				'section' 	=> 'menu_layout',
				'settings' 	=> 'etc_menu_bg_color',
				'label'	=> 'Select Menu Background Color',
				'description' => 'Must use colored logo',
		        'choices' => array(
					'white' => 'White',
		            'color' => 'Colored'
		        ),
			)
		)
	);

	// Set main nav position
	$wp_customize->add_setting(
		'etc_menu_position',
		array(
			'default'	=> __( 'right', 'etc' ),
		)
	);

	// Add options for navigation layout
	$wp_customize->add_control(
		new WP_Customize_Control(
			$wp_customize,
			'menu_position',
			array(
				'type'		=> 'select',
				'section' 	=> 'menu_layout',
				'settings' 	=> 'etc_menu_position',
				'label'	=> 'Select Menu Positioning',
		        'choices' => array(
					'right' => 'Right Alignment',
		            'left' => 'Left Alignment'
		        ),
			)
		)
	);
}

add_action( 'customize_register', 'etc_register_theme_customizer' );
