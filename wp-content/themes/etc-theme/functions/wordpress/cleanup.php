<?php 

// Add Category Id to body
function category_id_class($classes) {
    global $post;
    foreach((get_the_category($post->ID)) as $category)
        $classes [] = 'cat-' . $category->cat_ID . '-id';
        return $classes;
}
add_filter('post_class', 'category_id_class');
add_filter('body_class', 'category_id_class');

// Clean Titles
function clean_title($source) {
    $lowered = strtolower($source);
    $search = array(' ', '-');
    $replace = array('_', '_');
    $modded = str_replace($search, $replace, $lowered);

    return $modded;
}

// Line break adjustment
function clear_br($content){
    return str_replace("<br />","<br clear='none'/>", $content);
}
add_filter('the_content', 'clear_br');

// Clean URL's for attachments
function my_wp_get_attachment_url( $url, $post_id) {
    if ( $file = get_post_meta( $post_id, '_wp_attached_file', true) ) {
        if ( ($uploads = wp_upload_dir()) && false === $uploads['error'] ) {
            if ( file_exists( $uploads['basedir'] .'/'. $file ) ) {
                return $url;
            }
        }
    }
    return str_replace( WP_SITEURL, LIVE_SITEURL, $url );
}

// EditURI link.
remove_action( 'wp_head', 'rsd_link' );

// Category feed links.
remove_action( 'wp_head', 'feed_links_extra', 3 );

// Post and comment feed links.
remove_action( 'wp_head', 'feed_links', 2 );

// Windows Live Writer.
remove_action( 'wp_head', 'wlwmanifest_link' );

// Index link.
remove_action( 'wp_head', 'index_rel_link' );

// Previous link.
remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );

// Start link.
remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );

// Canonical.
remove_action( 'wp_head', 'rel_canonical', 10, 0 );

// Shortlink.
remove_action( 'wp_head', 'wp_shortlink_wp_head', 10, 0 );

// Links for adjacent posts.
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );

// WP version.
remove_action( 'wp_head', 'wp_generator' );

// Emoji detection script.
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );

// Emoji styles.
remove_action( 'wp_print_styles', 'print_emoji_styles' );