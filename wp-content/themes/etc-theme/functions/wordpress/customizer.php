<?php 

function etc_logo_customizer( $wp_customize ) {
	$wp_customize->add_section( 'etc_logo_section' , array(
	    'title'       => __( 'Logo', 'etc' ),
	    'priority'    => 30,
	    'description' => 'Upload a logo to replace the default site name and description in the header',
	) );

	$wp_customize->add_setting( 'etc_logo' );

	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'etc_logo', array(
	    'label'    => __( 'Logo Light', 'etc_logo' ),
	    'section'  => 'etc_logo_section',
	    'settings' => 'etc_logo',
	) ) );

	$wp_customize->add_setting( 'etc_logo_dark' );

	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'etc_logo_dark', array(
	    'label'    => __( 'Logo Dark', 'etc_logo_dark' ),
	    'section'  => 'etc_logo_section',
	    'settings' => 'etc_logo_dark',
	) ) );
}

add_action( 'customize_register', 'etc_logo_customizer' );

function etc_blog_options($wp_customize) {
	// Create custom panels
	$wp_customize->add_panel( 'blog_options', array(
	  'priority' => 1000,
	  'theme_supports' => '',
	  'title' => __( 'Blog Options', 'etc' ),
	  'description' => __( 'Controls the blog', 'etc' ),
	) );

	// Create custom field for mobile navigation layout
	$wp_customize->add_section( 'blog_options' , array(
		'title'	=> __('Blog Options','etc'),
		'panel' => 'blog_options',
		'priority' => 1000,
	));

	// Set default navigation layout
	$wp_customize->add_setting(
		'etc_blog_options',
		array(
			'default'	=> __( 'full', 'etc' ),
		)
	);

	// Add options for navigation layout
	$wp_customize->add_control(
		new WP_Customize_Control(
			$wp_customize,
			'blog_options',
			array(
				'type'		=> 'radio',
				'section' 	=> 'blog_options',
				'settings' 	=> 'etc_blog_options',
				'label'	=> 'Article Column Controler',
		        'choices' => array(
					'twelve' => 'Full Width',
		            'six' => 'Two Columns',
		            'four' => 'Three Columns',
		            'three' => 'Four Columns Full'
		        ),
			)
		)
	);

	$wp_customize->add_setting( 'post_per_page' );

	$wp_customize->add_control(
		new WP_Customize_Control(
			$wp_customize,
			'post_per_page',
		    array(
		        'label' => 'Articles Per Page',
		        'section' => 'blog_options',
		        'type' => 'select',
		        'choices'  => array(
					'4'  => 'Four',
					'6' => 'Six',
					'8'	=> 'Eight',
					'10'	=> 'Ten',
				),
		    )
		)
	);
}

add_action( 'customize_register', 'etc_blog_options' );

// Template Options
function etc_template_options($wp_customize) {
	// Create custom panels
	$wp_customize->add_panel( 'template_options', array(
	  'priority' => 1000,
	  'theme_supports' => '',
	  'title' => __( 'Template Options', 'etc' ),
	  'description' => __( 'Controls the template', 'etc' ),
	) );

	// Create custom field for mobile navigation layout
	$wp_customize->add_section( 'template_options' , array(
		'title'	=> __('Template Options','etc'),
		'panel' => 'template_options',
		'priority' => 1000,
	));

	// Set default navigation layout
	$wp_customize->add_setting(
		'etc_template_options',
		array(
			'default'	=> __( 'black', 'etc' ),
		)
	);

	// Add options for navigation layout
	$wp_customize->add_control(
		new WP_Customize_Control(
			$wp_customize,
			'template_options',
			array(
				'type'		=> 'select',
				'section' 	=> 'template_options',
				'settings' 	=> 'etc_template_options',
				'label'	=> 'Select Color Theme',
		        'choices' => array(
		        	'black' => 'Black',
					'red' => 'Red',
		            'blue' => 'Blue',
		            'green' => 'Green',
		            'orange' => 'Orange',
		            'violet' => 'Violet',
		            'turquoise' => 'Turquoise'
		        ),
			)
		)
	);

	// Set default navigation layout
	$wp_customize->add_setting(
		'etc_template_font',
		array(
			'default'	=> __( 'default', 'etc' ),
		)
	);

	// Add options for navigation layout
	$wp_customize->add_control(
		new WP_Customize_Control(
			$wp_customize,
			'template_font',
			array(
				'type'		=> 'select',
				'section' 	=> 'template_options',
				'settings' 	=> 'etc_template_font',
				'label'	=> 'Select Font Family',
		        'choices' => array(
					'default' => 'Open Sans',
		            'roboto' => 'Roboto',
		            'josefin' => 'Josefin Sans',
		            'ubuntu' => 'Ubuntu'
		        ),
			)
		)
	);
}

add_action( 'customize_register', 'etc_template_options' );