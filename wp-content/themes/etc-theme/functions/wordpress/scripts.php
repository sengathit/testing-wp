<?php 

// Load site scripts
function site_scripts() {
    wp_deregister_script('jquery');
    wp_deregister_script('jquery-ui-core');
    $pathToDependencies = THEME_JS . "/dependencies.js";
    $pathToScripts = THEME_JS . "/scripts.js";
    wp_enqueue_script('jquery', $pathToDependencies, array(), '', true);
    wp_enqueue_script( 'site_script', $pathToScripts, array('jquery') , '1.0', true );
}
add_action( 'wp_enqueue_scripts', 'site_scripts' );
