<?php
    
    //==============================================================
    // DIRECTORIES
    //==============================================================
    define( 'THEME_DIRECTORY', get_stylesheet_directory() );
    define( 'THEME_URI', get_stylesheet_directory_uri() );
    define( 'THEME_FUNCTION', THEME_URI . '/functions' );
    define( 'THEME_INCLUDE', THEME_DIRECTORY . '/includes' );
    define( 'THEME_IMAGES', THEME_URI . '/assets/images' );
    define( 'THEME_CSS', THEME_URI . '/assets/css' );
    define( 'THEME_JS', THEME_URI . '/assets/js' );
    define( 'THEME_VIDEO', THEME_URI . '/assets/video' );

    //==============================================================
    // LANDING PAGES
    //==============================================================
    include( TEMPLATEPATH . '/vendor/landing-pages/functions.php' );
   
    //==============================================================
    // CUSTOM POST TYPES
    //==============================================================
    require_once( TEMPLATEPATH . '/functions/cpt/custom-post-types.php' );
    
    //==============================================================
    // WORDPRESS CLEANUP
    //==============================================================
    require_once( TEMPLATEPATH . '/functions/wordpress/cleanup.php' );

    //==============================================================
    // WP RESPONSIVE IMAGES
    //==============================================================
    require_once( TEMPLATEPATH . '/functions/wordpress/responsive-images.php' );

    //==============================================================
    // ENQUEUE SCRIPTS
    //==============================================================
    require_once( TEMPLATEPATH . '/functions/wordpress/scripts.php' );

    //==============================================================
    // THEME SUPPORT
    //==============================================================
    require_once( TEMPLATEPATH . '/functions/wordpress/theme-support.php' );

    //==============================================================
    // BOOTSTRAP FUNCTIONS
    //==============================================================
    foreach (glob( TEMPLATEPATH . '/functions/bootstrap-functions/*.php') as $bootstrapFilename)
    {
        include $bootstrapFilename;
    }

    //==============================================================
    // MAILCHIMP FUNCTIONS
    //==============================================================
    require_once( TEMPLATEPATH . '/functions/mailchimp/mailchimp.php' );

    //==============================================================
    // MAILER FUNCTIONS
    //==============================================================
    require_once( TEMPLATEPATH . '/functions/mailer/mailer.php' );

    //==============================================================
    // WOOCOMMERCE FUNCTIONS
    //==============================================================
    // foreach (glob( TEMPLATEPATH . '/functions/woocommerce/*.php') as $woocommerceFilename)
    // {
    //     include $woocommerceFilename;
    // }