---

## ETC Base Theme - non ecommerce

Use this repository to quickly start all projects for non-ecommerce website builds. To clone the repository for ecommerce builds, go [here to view the repo](). In order to merge to master branch, all pull requests must be approved by Web manager. Hotfixies such as: styling updates, content, minor layout updates can be merged to master with out pull request. 

**COMMITS:**
When commiting your code, include short detail of task. 

## Wordpress Setup
1. Create site within MAMP and install wordpress
2. Copy theme framework into themes folder and remove all un-used themes
3. Enable Advanced Custom Fields
4. Create CPT based on requirements

## Styling
1. Establish default styling from style guide
2. Create partials for all Custom Post Types that were created
3. Run gulp

## Landing pages
1. 

[Bootstrap 4](https://getbootstrap.com/docs/4.0/getting-started/introduction/)

[Git](https://git-scm.com/)

[Gulp.js](https://gulpjs.com/)

[Sass](https://sass-lang.com/)


## Clone a repository
1. Create your MAMP install for the new WordPress site
2. Create repository with theme name ex. eighty-three-creative
3. CD into wp-content/themes/ folder
4. Clone Site repository and add base theme files
5. Git add, commit and push to new site repository

---

## Edit a file

You’ll start by editing this README file to learn how to edit a file in Bitbucket.

1. Click **Source** on the left side.
2. Click the README.md link from the list of files.
3. Click the **Edit** button.
4. Delete the following text: *Delete this line to make a change to the README from Bitbucket.*
5. After making your change, click **Commit** and then **Commit** again in the dialog. The commit page will open and you’ll see the change you just made.
6. Go back to the **Source** page.

---

## Create a file

Next, you’ll add a new file to this repository.

1. Click the **New file** button at the top of the **Source** page.
2. Give the file a filename of **contributors.txt**.
3. Enter your name in the empty file space.
4. Click **Commit** and then **Commit** again in the dialog.
5. Go back to the **Source** page.

Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages.

---



Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).