<?php get_header(); ?>

<main class="500error" role="main">
    <div class="container">
        <?php while ( have_posts() ) : the_post(); ?>
            <article id="post-<?php the_ID(); ?>">
               <header>
                   <h1 class="entry-title"><?php the_title(); ?></h1>
               </header>
               <div class="entry-content">
                   <p>Sorry, but the page you requested cannot be found.</p>
               </div>
            </article>
        <?php endwhile;?>
    </div>  
</main>

<?php get_footer(); ?>