<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress-testing');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '}j]Ku+xe![6K$&YCe|.PB|E+?lf|We?fe._[=fJo8N7VLn>WY,g/aC|HO^Bb-pze');
define('SECURE_AUTH_KEY',  'MLCin9Y+CO6cxhnD-*lb7@c9Ne)aUcjjshmb@{s7LlST~RV$KRf8z/>b_TL|G}KA');
define('LOGGED_IN_KEY',    'pl&}@Nmi,cLV?a&Wm%3|ec]@ZL;)DEFZ;kN?<)AOMxXE`&e-gDO7|S`y7P4K}sG#');
define('NONCE_KEY',        '4j-3G|!mSd uQn#Lpi3<g:p#7/+S[hl`]LMqA]BK2^,|+e.&^>0f-J~zyh$z-^l$');
define('AUTH_SALT',        'qK|N94{ha/7$`&pdtlOy+g|sR78u|`vE_am_S+Wz,fj+@s|]]u@!;Zf$LTg&O~5F');
define('SECURE_AUTH_SALT', 'B-ljn *#OtwQ_])WI%;|BP-{SQ@QtsfzUrb=|vvL?}B+>[87X-w{af9DC>NJZoH;');
define('LOGGED_IN_SALT',   '^iAyjOUGok8#=YFR(e6-rh[L84-rnWZ!{@X@<jSBg^;MDkrXh9-DT-jA4J],7D=[');
define('NONCE_SALT',       '/xUE|z!:A<*.&E<$8;V+05-X}^:|!I%t[yCqwF|}Gxvr}X>~K^syD^eI{F9ww^@I');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'etc_wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
